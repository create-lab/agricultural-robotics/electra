#include <ros.h>
#include <std_msgs/Int32.h>

#include <Arduino.h>

const uint8_t BRAKEVCC = 0;
const uint8_t CCW = 1;
const uint8_t CW = 2;
const uint8_t BRAKEGND = 3;

const int inAPin = 7;
const int inBPin = 8;
const int pwmPin = 5;

const int PWM_DEADBAND = 20;
const int PWM_MAX = 255;

ros::NodeHandle  nh;
void rosCommandCallback( const std_msgs::Int32& command);
ros::Subscriber<std_msgs::Int32> rosCommandSubscriber("/electra/motor/command", &rosCommandCallback );

void setup(){
  rosSetup();
  motorSetup();
}

void loop(){
  nh.spinOnce();
  delay(1);
}

void rosSetup(){
  nh.initNode();
  nh.subscribe(rosCommandSubscriber);
  while (!nh.connected()) {
      nh.spinOnce();
      delay(10);
  }
}

void rosCommandCallback( const std_msgs::Int32& command){
  uint8_t command_direction = command.data > 0 ? CW : CCW;
  uint8_t command_magnitude = abs(command.data);

  if (PWM_DEADBAND <= command_magnitude && command_magnitude <= PWM_MAX){
    motorGo(command_direction, command_magnitude);
  }
  else if (command_magnitude <= PWM_DEADBAND){
    motorGo(command_direction, 0);
  } 
  else if (PWM_MAX <= command_magnitude){
    nh.logerror("Invalid command: |255|<|command|");
  } 
  else{
    nh.logerror("Invalid command");
  }
}

void motorSetup(){
  pinMode(inAPin, OUTPUT);
  pinMode(inBPin, OUTPUT);
  pinMode(pwmPin, OUTPUT);
  
  digitalWrite(inAPin, LOW);
  digitalWrite(inBPin, LOW);
}

void motorOff(){
  digitalWrite(inAPin, LOW);
  digitalWrite(inBPin, LOW);
  analogWrite(pwmPin, 0);
}

void motorGo(uint8_t mode, uint8_t speed){
  switch (mode){
  case BRAKEVCC: // Brake to VCC
    digitalWrite(inAPin, HIGH);
    digitalWrite(inBPin, HIGH);
    break;
  case CW: // Turn Clockwise
    digitalWrite(inAPin, LOW);
    digitalWrite(inBPin, HIGH);
    break;
  case CCW: // Turn Counter-Clockwise
    digitalWrite(inAPin, HIGH);
    digitalWrite(inBPin, LOW);
    break;
  case BRAKEGND: // Brake to GND
    digitalWrite(inAPin, LOW);
    digitalWrite(inBPin, LOW);
    break;

  default:
    // Invalid mode does not change the PWM signal
    return;
  }
  analogWrite(pwmPin, speed);
  return;
}