#include <electra_hardware_interface/electra_hardware_interface.h>

int main(int argc, char** argv) {
    ros::init(argc, argv, "electra_hardware_interface");
    ros::NodeHandle nh;
    ros::MultiThreadedSpinner spinner(2); 
    ElectraHardwareInterface electra(nh);
    spinner.spin();
    return 0;
}
