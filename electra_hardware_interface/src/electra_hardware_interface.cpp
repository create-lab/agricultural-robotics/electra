#include <electra_hardware_interface/electra_hardware_interface.h>

ElectraHardwareInterface::ElectraHardwareInterface(ros::NodeHandle& nh) : nh_(nh) {
    init();
    controller_manager_.reset(new controller_manager::ControllerManager(this, nh_));
    loop_hz_= 50.0;
    ros::Duration update_freq = ros::Duration(1.0/loop_hz_);
	
	command_publisher = nh_.advertise<std_msgs::Int32>("/electra/motor/command",10);
    encoder_subscriber = nh_.subscribe("/electra/encoder/raw", 1, &ElectraHardwareInterface::encoder_callback, this);
    loadcell_subscriber = nh_.subscribe("/electra/loadcell/raw", 1, &ElectraHardwareInterface::loadcell_callback, this);

    non_realtime_loop_ = nh_.createTimer(update_freq, &ElectraHardwareInterface::update, this);
}

void ElectraHardwareInterface::encoder_callback(const electra_msgs::StateConstPtr& msg){
    joint_position_ = msg->position;
    joint_velocity_ = msg->velocity;
}

void ElectraHardwareInterface::loadcell_callback(const std_msgs::Float32ConstPtr& msg){
    joint_effort_ = msg->data;
}

void ElectraHardwareInterface::init() {   
	joint_name_="electra";
    
    // Create joint state interface
    hardware_interface::JointStateHandle jointStateHandle(joint_name_, &joint_position_, &joint_velocity_, &joint_effort_);
    joint_state_interface_.registerHandle(jointStateHandle);

    // Create effort joint interface
    hardware_interface::JointHandle jointEffortHandle(jointStateHandle, &joint_effort_command_);
	effort_joint_interface_.registerHandle(jointEffortHandle);
	
    // Create Joint Limit interface   
    joint_limits_interface::JointLimits limits;
    joint_limits_interface::getJointLimits(joint_name_, nh_, limits);
	joint_limits_interface::EffortJointSaturationHandle jointLimitsHandle(jointEffortHandle, limits);
	effortJointSaturationInterface.registerHandle(jointLimitsHandle);
	
	
    // Register all joints interfaces    
    registerInterface(&joint_state_interface_);
    registerInterface(&effort_joint_interface_);
    registerInterface(&effortJointSaturationInterface);
}

void ElectraHardwareInterface::update(const ros::TimerEvent& e) {
    elapsed_time_ = ros::Duration(e.current_real - e.last_real);
    controller_manager_->update(ros::Time::now(), elapsed_time_);
    write(elapsed_time_);
}


void ElectraHardwareInterface::write(ros::Duration elapsed_time) {
    effortJointSaturationInterface.enforceLimits(elapsed_time);
    std_msgs::Int32 motorCommand;
    motorCommand.data = (int) joint_effort_command_;	
	command_publisher.publish(motorCommand);		
}
