#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <joint_limits_interface/joint_limits.h>
#include <joint_limits_interface/joint_limits_interface.h>
#include <joint_limits_interface/joint_limits_rosparam.h>
#include <joint_limits_interface/joint_limits_urdf.h>
#include <controller_manager/controller_manager.h>
#include <boost/scoped_ptr.hpp>
#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <electra_msgs/State.h>
#include <angles/angles.h>

class ElectraHardwareInterface : public hardware_interface::RobotHW 
{
	public:
        ElectraHardwareInterface(ros::NodeHandle& nh);
        ~ElectraHardwareInterface(){};
        void init();
        void update(const ros::TimerEvent& e);
        void write(ros::Duration elapsed_time);
        ros::Publisher command_publisher;
        ros::Subscriber encoder_subscriber;
        ros::Subscriber loadcell_subscriber;
        void encoder_callback(const electra_msgs::StateConstPtr& msg);
        void loadcell_callback(const std_msgs::Float32ConstPtr& msg);

    protected:
        hardware_interface::JointStateInterface joint_state_interface_;
        hardware_interface::EffortJointInterface effort_joint_interface_;

        
        joint_limits_interface::EffortJointSaturationInterface effortJointSaturationInterface;
        
        std::string joint_name_;  
        double joint_position_=0.0;
        double joint_velocity_=0.0;
        double joint_effort_=0.0;
        double joint_position_command_;
        double joint_effort_command_;
        double joint_velocity_command_;
        
        ros::NodeHandle nh_;
        ros::Timer non_realtime_loop_;
        ros::Duration elapsed_time_;
        double loop_hz_;
        boost::shared_ptr<controller_manager::ControllerManager> controller_manager_;
};

