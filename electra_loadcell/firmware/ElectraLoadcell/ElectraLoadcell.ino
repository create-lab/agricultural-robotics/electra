#include <ros.h>
#include <std_msgs/Float32.h>
#include <std_srvs/Empty.h>

#include <Arduino.h>

#include <HX711.h>
#include <EEPROM.h>

HX711 loadcell;
const int LOADCELL_DAT_PIN = 2;
const int LOADCELL_CLK_PIN = 3;
const long LOADCELL_DIVIDER = 215625;

const int EEPROM_ADDRESS = 0;

ros::NodeHandle nh;
std_msgs::Float32 loadcellMsg;
ros::Publisher loadcellPublisher("/electra/loadcell/raw", &loadcellMsg);

void tare_callback(const std_srvs::EmptyRequest &req, std_srvs::EmptyResponse &res);
ros::ServiceServer<std_srvs::EmptyRequest, std_srvs::EmptyResponse> tareServiceServer("/electra/loadcell/tare", &tare_callback);

void setup()
{
  nh.initNode();
  nh.advertise(loadcellPublisher);
  nh.advertiseService<std_srvs::EmptyRequest, std_srvs::EmptyResponse>(tareServiceServer);
  while (!nh.connected())
  {
    nh.spinOnce();
    delay(10);
  }

  loadcell.begin(LOADCELL_DAT_PIN, LOADCELL_CLK_PIN);
  loadcell.set_scale(LOADCELL_DIVIDER);

  long loadcell_offset;
  EEPROM.get(EEPROM_ADDRESS, loadcell_offset);
  loadcell.set_offset(loadcell_offset);
}

void tare_callback(const std_srvs::EmptyRequest &req, std_srvs::EmptyResponse &res)
{
  loadcell.tare();
  long loadcell_offset = loadcell.get_offset();
  EEPROM.put(EEPROM_ADDRESS, loadcell_offset);
}

void loop()
{
  if (loadcell.wait_ready_timeout(200))
  {
    float load = loadcell.get_units(1);
    loadcellMsg.data = -1.0 * load;
    loadcellPublisher.publish(&loadcellMsg);
  }
  else
  {
    nh.logerror("HX711 not found.");
  }
  nh.spinOnce();
}
