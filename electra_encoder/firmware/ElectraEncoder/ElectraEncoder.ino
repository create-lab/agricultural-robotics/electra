#include <ros.h>
#include <std_msgs/Int16.h>
#include <electra_msgs/State.h>

#include <Arduino.h>

#include <ams_as5048b.h>


AMS_AS5048B myEncoder;
const float ENCODER_SCALE = 7200.0 / 5289.0;

float lastEncoderAngle = 0.0;
float lastAbsoluteEncoderAngle = 0.0;
unsigned long lastEncoderTime = millis();
int16_t encoderTurns = 0;
float lastEncoderVelocity = 0.0;

ros::NodeHandle  nh;
electra_msgs::State rawMsg;
ros::Publisher encoderRawPublisher("/electra/encoder/raw", &rawMsg);

void setup() {
  nh.initNode();
  nh.advertise(encoderRawPublisher);
  while (!nh.connected()) {
      nh.spinOnce();
      delay(10);
  }

  myEncoder.begin();
  myEncoder.setClockWise(true); 
  myEncoder.setZeroReg(); 
}

float toSec(unsigned long milliseconds){
  return milliseconds / 1000.0;
}

void loop() { 
  myEncoder.updateMovingAvgExp(); //otherwise Arduino returns zero
  float currentEncoderAngle = myEncoder.angleR(U_RAD, false) ;
  unsigned long currentEncoderTime = millis();

  if(lastEncoderAngle <= M_PI/2.0 && currentEncoderAngle > M_PI*3.0/2.0){
      encoderTurns -= 1;
  }
  else if(lastEncoderAngle >= M_PI*3.0/2.0 && currentEncoderAngle < M_PI/2.0){
      encoderTurns += 1;
  }

  lastEncoderAngle = currentEncoderAngle;

  float currentAbsoluteEncoderAngle = encoderTurns * 2.0 * M_PI + currentEncoderAngle;
  float scaledAbsoluteEncoderAngle = currentAbsoluteEncoderAngle * ENCODER_SCALE;
  rawMsg.position = scaledAbsoluteEncoderAngle;

  float currentEncoderVelocity = (currentAbsoluteEncoderAngle - lastAbsoluteEncoderAngle) / 
                          toSec(currentEncoderTime - lastEncoderTime);

  currentEncoderVelocity = 0.01 * currentEncoderVelocity + 0.99 * lastEncoderVelocity;


  float scaledEncoderVelocity = currentEncoderVelocity * ENCODER_SCALE;
  rawMsg.velocity = scaledEncoderVelocity;

  lastAbsoluteEncoderAngle = currentAbsoluteEncoderAngle;
  lastEncoderTime = currentEncoderTime;
  lastEncoderVelocity = currentEncoderVelocity;

  encoderRawPublisher.publish( &rawMsg );
  nh.spinOnce();
}