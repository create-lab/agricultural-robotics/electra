#include <electra_controllers/force_controller.h>
#include <pluginlib/class_list_macros.hpp>

namespace electra_controllers
{

  ForceController::ForceController()
      : command_(0), loop_count_(0)
  {
  }

  ForceController::~ForceController()
  {
    sub_command_.shutdown();
  }

  bool ForceController::init(hardware_interface::EffortJointInterface *robot,
                             const std::string &joint_name, const control_toolbox::Pid &pid)
  {
    pid_controller_ = pid;

    // Get joint handle from hardware interface
    joint_ = robot->getHandle(joint_name);

    return true;
  }

  bool ForceController::init(hardware_interface::EffortJointInterface *robot, ros::NodeHandle &n)
  {
    // Get joint name from parameter server
    std::string joint_name;
    if (!n.getParam("joint", joint_name))
    {
      ROS_ERROR("No joint given (namespace: %s)", n.getNamespace().c_str());
      return false;
    }

    // Get joint handle from hardware interface
    joint_ = robot->getHandle(joint_name);

    // Load PID Controller using gains set on parameter server
    if (!pid_controller_.init(ros::NodeHandle(n, "pid")))
      return false;

    // Start realtime state publisher
    controller_state_publisher_.reset(
        new realtime_tools::RealtimePublisher<control_msgs::JointControllerState>(n, "state", 1));

    // Start command subscriber
    sub_command_ = n.subscribe<std_msgs::Float64>("command", 1, &ForceController::setCommandCallback, this);

    return true;
  }

  void ForceController::setGains(const double &p, const double &i, const double &d, const double &i_max, const double &i_min, const bool &antiwindup)
  {
    pid_controller_.setGains(p, i, d, i_max, i_min, antiwindup);
  }

  void ForceController::getGains(double &p, double &i, double &d, double &i_max, double &i_min, bool &antiwindup)
  {
    pid_controller_.getGains(p, i, d, i_max, i_min, antiwindup);
  }

  void ForceController::getGains(double &p, double &i, double &d, double &i_max, double &i_min)
  {
    bool dummy;
    pid_controller_.getGains(p, i, d, i_max, i_min, dummy);
  }

  void ForceController::printDebug()
  {
    pid_controller_.printValues();
  }

  std::string ForceController::getJointName()
  {
    return joint_.getName();
  }

  // Set the joint velocity command
  void ForceController::setCommand(double cmd)
  {
    command_ = cmd;
  }

  // Return the current velocity command
  void ForceController::getCommand(double &cmd)
  {
    cmd = command_;
  }

  void ForceController::starting(const ros::Time &time)
  {
    command_ = 0.3;
    pid_controller_.reset();
  }

  void ForceController::update(const ros::Time &time, const ros::Duration &period)
  {
    double error = command_ - joint_.getEffort();

    // Set the PID error and compute the PID command with nonuniform time
    // step size. The derivative error is computed from the change in the error
    // and the timestep dt.
    double commanded_effort = pid_controller_.computeCommand(error, period);

    joint_.setCommand(commanded_effort);

    if (loop_count_ % 3 == 0)
    {
      if (controller_state_publisher_ && controller_state_publisher_->trylock())
      {
        controller_state_publisher_->msg_.header.stamp = time;
        controller_state_publisher_->msg_.set_point = command_;
        controller_state_publisher_->msg_.process_value = joint_.getEffort();
        controller_state_publisher_->msg_.error = error;
        controller_state_publisher_->msg_.time_step = period.toSec();
        controller_state_publisher_->msg_.command = commanded_effort;

        double dummy;
        bool antiwindup;
        getGains(controller_state_publisher_->msg_.p,
                 controller_state_publisher_->msg_.i,
                 controller_state_publisher_->msg_.d,
                 controller_state_publisher_->msg_.i_clamp,
                 dummy,
                 antiwindup);
        controller_state_publisher_->msg_.antiwindup = static_cast<char>(antiwindup);
        controller_state_publisher_->unlockAndPublish();
      }
    }
    loop_count_++;
  }

  void ForceController::setCommandCallback(const std_msgs::Float64ConstPtr &msg)
  {
    command_ = msg->data;
  }

} // namespace

PLUGINLIB_EXPORT_CLASS(electra_controllers::ForceController, controller_interface::ControllerBase)
