#! /usr/bin/env python3

from typing import Any
import math

import rospy
import std_msgs.msg
import sensor_msgs.msg

from create_common_rospy.simple_subscriber import SimpleSubscriber

class ElectraTetherControl:
    def __init__(self) -> None:
        self.arm2gravity_beta_subscriber = SimpleSubscriber("/rover/arm2gravity_beta/encoder/rad", std_msgs.msg.Float32)
        self.inclination_alpha_subscriber = SimpleSubscriber("/rover/inclination_alpha/rad", std_msgs.msg.Float32)

        self.leadout_subscriber = SimpleSubscriber("/rover/leadout_encoder/raw", std_msgs.msg.Float32)
    
        self.command_publisher = rospy.Publisher("/electra/force_controller/command", std_msgs.msg.Float64, queue_size=10)

        timer_period = rospy.Duration(0.04)
        self.timer = rospy.Timer(timer_period, self.timer_callback)

    def timer_callback(self, event: rospy.timer.TimerEvent) -> None:
        beta = self.arm2gravity_beta_subscriber.message.data
        alpha = self.inclination_alpha_subscriber.message.data
        
        rover_mass = 14.25
        tether_force = rover_mass * math.cos(beta) * math.sin(alpha)

        command_msg = std_msgs.msg.Float64()
        command_msg.data = tether_force
        self.command_publisher.publish(command_msg)
