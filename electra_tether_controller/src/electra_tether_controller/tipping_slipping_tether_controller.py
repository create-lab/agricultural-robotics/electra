#! /usr/bin/env python3

from typing import Any
import math
import rospy
import std_msgs.msg
import sensor_msgs.msg
from scipy.optimize import minimize_scalar
import numpy as np

from create_common_rospy.simple_subscriber import SimpleSubscriber

class TippingSlippingTetherControl:
    def __init__(self) -> None:
        self.arm2gravity_beta_subscriber = SimpleSubscriber("/rover/arm2gravity_beta/encoder/rad", std_msgs.msg.Float32)
        self.inclination_alpha_subscriber = SimpleSubscriber("/rover/inclination_alpha/rad", std_msgs.msg.Float32)

        self.leadout_subscriber = SimpleSubscriber("/rover/leadout_encoder/raw", std_msgs.msg.Float32)
    
        self.command_publisher = rospy.Publisher("/electra/force_controller/command", std_msgs.msg.Float64, queue_size=10)

        self.height_tether = rospy.get_param("/height_tether")
        rospy.logwarn(f"The tether height is: {self.height_tether}")

        timer_period = rospy.Duration(0.04)
        self.timer = rospy.Timer(timer_period, self.timer_callback)

    def timer_callback(self, event: rospy.timer.TimerEvent) -> None:
        beta = self.arm2gravity_beta_subscriber.message.data
        alpha = self.inclination_alpha_subscriber.message.data
        
        F_gravity = 14.25
        mu = 0.25

        F_tether_for_F_inplane_min = F_gravity * math.cos(beta) * math.sin(alpha) 
        try:
            F_tether_min = F_tether_for_F_inplane_min - F_gravity * math.sqrt(math.cos(alpha) * math.cos(alpha) * mu * mu - math.sin(alpha)*math.sin(alpha) * math.sin(beta)*math.sin(beta))
        except:
            rospy.logwarn_throttle(1,"Can't prevent slipping.")
            F_tether_min = F_tether_for_F_inplane_min 
        rospy.loginfo_throttle(1, f"F_tether_inplane_min: {F_tether_min}")

        robot_tip = 0.35; #0.3
        height_gravity = 0.14

        def F_tip_num_phi(phi):
            f1 = F_gravity * (robot_tip*math.cos(alpha)+2*height_gravity*math.sin(alpha)*math.cos(phi))/ (2*self.height_tether*math.cos(beta-phi))
            f2 = F_gravity * (-robot_tip*math.cos(alpha)+2*height_gravity*math.sin(alpha)*math.cos(phi))/ (2*self.height_tether*math.cos(beta-phi))
            return np.maximum(np.maximum(f1, f2),0)
        phi_bounds = ((-np.pi,np.pi),)
        F_tip_max = minimize_scalar(F_tip_num_phi, bounds=phi_bounds).fun       
        rospy.loginfo_throttle(1, f"F_tip_max: {F_tip_max}")

        slip_margin = 0.7

        F_ref = (F_tip_max + slip_margin + F_tether_min)/2.0
        F_ref = np.minimum(F_ref, F_tether_for_F_inplane_min)
        F_ref = np.minimum(F_ref, F_tip_max)
        rospy.loginfo_throttle(1, f"F_ref: {F_ref}")

        command_msg = std_msgs.msg.Float64()
        command_msg.data = F_ref
        self.command_publisher.publish(command_msg)
